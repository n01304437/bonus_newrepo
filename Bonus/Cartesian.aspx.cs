﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {

            int x = int.Parse(X_Text.Text);
            int y = int.Parse(Y_Text.Text);
            if (x > 0 && y > 0)
            {

                output.InnerHtml = " Value of X and Y fall in first quadrant";
            }
            else if (x < 0 && y > 0)
            {
                output.InnerHtml = " Value of X and Y fall in Second quadrant";
            }
            else if (x < 0 && y < 0)
            {
                output.InnerHtml = " Value of X and Y fall in third quadrant";
            }
            else
            {
                output.InnerHtml = " Value of X and Y fall in forth quadrant";
            }


        }
    }
}