﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus
{
    public partial class String_Bling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string str = stringText.Text;
            str = str.ToLower();
            string removeSpc = str.Replace(" ", string.Empty);
            int length = removeSpc.Length;
            int k = 1;
            for (int i = 0; i < length / 2; i++)
            {
                if (removeSpc[i] != removeSpc[length - i - 1])
                {
                    k = 0;
                    break;
                }
            }
            if (k == 1)
            {
                palindrome.InnerHtml = "String is palindrom";
            }
            else
            {
                palindrome.InnerHtml = "String is not palindrom";
            }
        }
    }
}