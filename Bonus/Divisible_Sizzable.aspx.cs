﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus
{
    public partial class Divisible_Sizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            int x = int.Parse(X_Text.Text);
            int k = 1;
            for (int i = 2; i < x; i++)
            {
                if (x % i == 0)
                {
                    k = 0;
                    break;
                }
            }
            if (k == 1)
            {
                prime.InnerHtml = "X is Prime number";
            }
            else
            {
                prime.InnerHtml = "X is not Prime number";
            }
        }
    }
}