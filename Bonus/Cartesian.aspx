﻿
<%@ Page Title="Cartesian Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="Bonus.Cartesian" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    
        <div>
            
            <h3>Cartesian</h3>
            <asp:Label ID="X_Value" runat="server" Text="X"></asp:Label>
            <asp:TextBox ID="X_Text" runat="server"></asp:TextBox>
            &nbsp;<asp:RegularExpressionValidator ID="X_Valid" runat="server" ControlToValidate="X_Text" ErrorMessage="invalid input" ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Label ID="Y_Value" runat="server" Text="Y"></asp:Label>

            <asp:TextBox ID="Y_Text" runat="server"></asp:TextBox>
            &nbsp;<asp:RegularExpressionValidator ID="Y_Valid" runat="server" ControlToValidate="Y_Text" ErrorMessage="invalid input" ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Button ID="Submit" runat="server" Text="SUBMIT" OnClick="Submit_Click" />

            
                <div id="output" runat="server">

                </div>

        </div>

</asp:Content>